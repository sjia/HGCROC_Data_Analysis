#include "easylogging++.h"
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TCanvas.h"
#include "TH2.h"
#include "TH1.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "SJH_frame.h"

INITIALIZE_EASYLOGGINGPP

void set_easylogger(); // set easylogging++ configurations

int main(int argc, char* argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    set_easylogger();

    auto file_name_input  = "dump/raw_analysis/Scan_20231017_002_beam/analysis_Scanning_20231017_174818_Delay10.root";
    auto file_name_output = "dump/raw_analysis/analysis_20231012_090906_172.root";

    int opt;
    while ((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt){
            case 'i':
                file_name_input = optarg;
                break;
            case 'o':
                file_name_output = optarg;
                break;
            default:
                LOG(ERROR) << "Wrong arguments!";
                return 1;
        }
    }

    LOG(INFO) << "Input file: " << file_name_input;
    LOG(INFO) << "Output file: " << file_name_output;

    std::string file_name_str(file_name_input);
    std::string delay_val_str = file_name_str.substr(file_name_str.find("Delay") + 5, 2);

    int bin_num_1d = 200;

    auto input_root_file_ptr = TFile::Open(file_name_input, "READ");
    if (!input_root_file_ptr){
        LOG(ERROR) << "Cannot open input file!";
        return 1;
    }

    auto tree_ptr = (TTree*)input_root_file_ptr->Get("HF_T");
    if (!tree_ptr){
        LOG(ERROR) << "Cannot find TTree HGCROC_Frames!";
        return 1;
    }

    HGCROC_Frame* HGCROC_Frame_ptr = nullptr;

    tree_ptr->SetBranchAddress("HGCROC_Frames", &HGCROC_Frame_ptr);

    auto _tree_entries = tree_ptr->GetEntries();
    LOG(INFO) << "Total entries: " << _tree_entries;

    std::vector<HGCROC_Frame> _frames;

    for (auto i = 0; i < _tree_entries; i++){
        tree_ptr->GetEntry(i);
        _frames.push_back(*HGCROC_Frame_ptr);
    }
    input_root_file_ptr->Close();

    LOG(INFO) << "Total frames: " << _frames.size();

    auto output_root_file_ptr = TFile::Open(file_name_output, "RECREATE");
    if (!output_root_file_ptr){
        LOG(ERROR) << "Cannot open output file!";
        return 1;
    }
    output_root_file_ptr->mkdir("channel_hist_val0");
    output_root_file_ptr->mkdir("channel_hist_val1");
    output_root_file_ptr->mkdir("channel_hist_val2");

    // * --- channel adc distribution --- *
    std::vector<uint16_t> _channel_val_0;
    std::vector<uint16_t> _channel_val_1;
    std::vector<uint16_t> _channel_val_2;
    _channel_val_0.reserve(_frames.size() * 76);
    _channel_val_1.reserve(_frames.size() * 76);
    _channel_val_2.reserve(_frames.size() * 76);

    for (auto& _frame : _frames){
        if (_frame.get_DaqH_H1(0) != 0 || _frame.get_DaqH_H2(0) != 0 || _frame.get_DaqH_H3(0) != 0){
            continue;
        }
        if (_frame.get_DaqH_H1(1) != 0 || _frame.get_DaqH_H2(1) != 0 || _frame.get_DaqH_H3(1) != 0){
            continue;
        }
        for (auto& _chn : _frame._data){
            _channel_val_0.push_back(_chn.val_10b_0);
            _channel_val_1.push_back(_chn.val_10b_1);
            _channel_val_2.push_back(_chn.val_10b_2);
        }
    }

    std::vector<TH1D*> _channel_hist_val_0;
    std::vector<TH1D*> _channel_hist_val_1;
    std::vector<TH1D*> _channel_hist_val_2;

    for (auto i = 0; i < 76; i++){
        auto _hist_name = "hist_v0_channel_" + std::to_string(i);
        auto _hist_title = "hist_v0_channel_" + std::to_string(i);
        auto _hist = new TH1D(_hist_name.c_str(), _hist_title.c_str(), 1024, 0, 1024);
        _channel_hist_val_0.push_back(_hist);
    }

    for (auto i = 0; i < 76; i++){
        auto _hist_name = "hist_v1_channel_" + std::to_string(i);
        auto _hist_title = "hist_v1_channel_" + std::to_string(i);
        auto _hist = new TH1D(_hist_name.c_str(), _hist_title.c_str(), 1024, 0, 1024);
        _channel_hist_val_1.push_back(_hist);
    }

    for (auto i = 0; i < 76; i++){
        auto _hist_name = "hist_v2_channel_" + std::to_string(i);
        auto _hist_title = "hist_v2_channel_" + std::to_string(i);
        auto _hist = new TH1D(_hist_name.c_str(), _hist_title.c_str(), 1024, 0, 1024);
        _channel_hist_val_2.push_back(_hist);
    }

    auto canvas_val_0 = new TCanvas("canvas_val_0", "canvas_val_0", 1400, 1000);
    auto hist2d_val_0 = new TH2I("hist2d_val_0", "hist2d_val_0", 76, 0, 76, 256, 0, 1024);
    for (auto i = 0; i < _channel_val_0.size(); i++){
        hist2d_val_0->Fill(i%76, _channel_val_0[i]);
        _channel_hist_val_0[i%76]->Fill(_channel_val_0[i]);
    }

    output_root_file_ptr->cd("channel_hist_val0");
    for (auto& _hist : _channel_hist_val_0){
        auto _canvas = new TCanvas(_hist->GetName(), _hist->GetTitle(), 1000, 700);
        _hist->GetXaxis()->SetRangeUser(40, 160);
        _hist->SetFillColor(kBlue - 10);
        _hist->SetLineColor(kBlue);
        _hist->SetLineWidth(3);
        _hist->SetTitle("");
        _hist->GetXaxis()->SetTitle("ADC");
        _hist->GetYaxis()->SetTitle("Count");
        _hist->Draw("hist");
        _canvas->SetLogy();
        _canvas->SetGrid();
        // set statistics box
        _hist->SetStats(0);
        // set statistics box to the top right
        TLatex _latex;
        _latex.SetTextSize(0.04);
        _latex.SetTextAlign(31);
        _latex.SetTextColor(kBlack);
        _latex.DrawLatex(130, 14000,    "H2GCROC External Injection ");
        _latex.SetTextSize(0.035);
        _latex.DrawLatex(130, 7000,     "Channel 2 without Sr source ");
        _latex.DrawLatex(130, 4000,     "Oct. 2023 ");

        std::string _entry_num_str = "Entries: " + std::to_string(int(_hist->GetEntries()));
        std::string _mean_str = "Mean: " + std::to_string(_hist->GetMean());
        std::string _rms_str = "RMS: " + std::to_string(_hist->GetRMS());
        
        TPaveText _pt(0.7, 0.7, 0.9, 0.9, "NDC");
        _pt.SetFillColor(kWhite);
        _pt.SetBorderSize(3);
        _pt.SetTextAlign(12);
        _pt.SetTextSize(0.03);
        _pt.AddText(_entry_num_str.c_str());
        _pt.AddText(_mean_str.c_str());
        _pt.AddText(_rms_str.c_str());
        _pt.Draw();
        // set log y
        // _canvas->SaveAs(("dump/raw_analysis/" + std::string(_hist->GetName()) + ".png").c_str());
        _canvas->Write();
    }
    _channel_hist_val_0.clear();

    canvas_val_0->cd();
    hist2d_val_0->SetTitle("");
    hist2d_val_0->SetStats(0);
    hist2d_val_0->GetXaxis()->SetTitle("Channel");
    hist2d_val_0->GetYaxis()->SetTitle("ADC");
    hist2d_val_0->Draw("colz");
    // canvas_val_0->SetGrid();
    output_root_file_ptr->cd();
    canvas_val_0->SetLogz();
    canvas_val_0->Write();
    if (delay_val_str == "10")
        canvas_val_0->SaveAs("dump/raw_analysis/hist2d_val_0.png");
    
    auto canvas_val_1 = new TCanvas("canvas_val_1", "canvas_val_1", 800, 600);
    auto hist2d_val_1 = new TH2I("hist2d_val_1", "hist2d_val_1", 76, 0, 76, 1024, 0, 1024);

    for (auto i = 0; i < _channel_val_1.size(); i++){
        hist2d_val_1->Fill(i%76, _channel_val_1[i]);
        _channel_hist_val_1[i%76]->Fill(_channel_val_1[i]);
    }

    output_root_file_ptr->cd("channel_hist_val1");
    for (auto& _hist : _channel_hist_val_1){
        _hist->Write();
    }
    _channel_hist_val_1.clear();

    hist2d_val_1->Draw("colz");
    canvas_val_1->SetGrid();
    output_root_file_ptr->cd();
    canvas_val_1->Write();

    auto canvas_val_2 = new TCanvas("canvas_val_2", "canvas_val_2", 800, 600);
    auto hist2d_val_2 = new TH2I("hist2d_val_2", "hist2d_val_2", 76, 0, 76, 1024, 0, 1024);

    for (auto i = 0; i < _channel_val_2.size(); i++){
        hist2d_val_2->Fill(i%76, _channel_val_2[i]);
        _channel_hist_val_2[i%76]->Fill(_channel_val_2[i]);
    }

    output_root_file_ptr->cd("channel_hist_val2");
    for (auto& _hist : _channel_hist_val_2){
        _hist->Write();
    }
    _channel_hist_val_2.clear();

    hist2d_val_2->Draw("colz");
    canvas_val_2->SetGrid();
    output_root_file_ptr->cd();
    canvas_val_2->Write();

    _channel_val_0.clear();
    _channel_val_1.clear();
    _channel_val_2.clear();

    canvas_val_0->Close();
    canvas_val_1->Close();
    canvas_val_2->Close();

    // * --- Hamming code error rate --- *
    std::vector<uint8_t> _hamming_code_err_list;
    _hamming_code_err_list.reserve(_frames.size());

    for (auto& _frame : _frames){
        int _error_cnt = 0;
        _error_cnt += _frame.get_DaqH_H1(0);
        _error_cnt += _frame.get_DaqH_H1(1);
        _error_cnt += _frame.get_DaqH_H2(2);
        _error_cnt += _frame.get_DaqH_H2(3);
        _error_cnt += _frame.get_DaqH_H3(4);
        _error_cnt += _frame.get_DaqH_H3(5);
        _hamming_code_err_list.push_back(_error_cnt);
    }

    auto canvas_hamming_code_err = new TCanvas("canvas_hamming_code_err", "canvas_hamming_code_err", 800, 600);
    auto hist_hamming_code_err = new TH1I("hist_hamming_code_err", "hist_hamming_code_err", 7, 0, 7);

    for (auto& _err : _hamming_code_err_list){
        hist_hamming_code_err->Fill(_err);
    }

    hist_hamming_code_err->GetXaxis()->SetTitle("Hamming code error count");
    hist_hamming_code_err->GetYaxis()->SetTitle("Count");
    hist_hamming_code_err->SetFillColor(kBlue - 10);
    hist_hamming_code_err->SetLineColor(kBlue);
    hist_hamming_code_err->SetLineWidth(2);

    hist_hamming_code_err->Draw("hist");
    canvas_hamming_code_err->SetGrid();
    canvas_hamming_code_err->SetLogy();
    canvas_hamming_code_err->Write();

    _hamming_code_err_list.clear();
    canvas_hamming_code_err->Close();

    // * --- Event Val Sum Distribution --- *
    std::vector<Double_t> _event_val_0_sum_list;
    std::vector<Double_t> _event_val_1_sum_list;
    std::vector<Double_t> _event_val_2_sum_list;

    _event_val_0_sum_list.reserve(_frames.size());
    _event_val_1_sum_list.reserve(_frames.size());
    _event_val_2_sum_list.reserve(_frames.size());

    for (auto& _frame : _frames){
        Double_t _val_0_sum = 0;
        Double_t _val_1_sum = 0;
        Double_t _val_2_sum = 0;
        for (auto& _chn : _frame._data){
            _val_0_sum += _chn.val_10b_0;
            _val_1_sum += _chn.val_10b_1;
            _val_2_sum += _chn.val_10b_2;
        }
        _event_val_0_sum_list.push_back(_val_0_sum);
        _event_val_1_sum_list.push_back(_val_1_sum);
        _event_val_2_sum_list.push_back(_val_2_sum);
    }

    std::sort(_event_val_0_sum_list.begin(), _event_val_0_sum_list.end());
    std::sort(_event_val_1_sum_list.begin(), _event_val_1_sum_list.end());
    std::sort(_event_val_2_sum_list.begin(), _event_val_2_sum_list.end());

    auto canvas_event_val_0_sum = new TCanvas("canvas_event_val_0_sum", "canvas_event_val_sum", 800, 600);
    auto canvas_event_val_1_sum = new TCanvas("canvas_event_val_1_sum", "canvas_event_val_sum", 800, 600);
    auto canvas_event_val_2_sum = new TCanvas("canvas_event_val_2_sum", "canvas_event_val_sum", 800, 600);

    auto _val_0_min = _event_val_0_sum_list[0.01 * _event_val_0_sum_list.size()];
    auto _val_0_max = _event_val_0_sum_list[0.99 * _event_val_0_sum_list.size()];
    auto _val_1_min = _event_val_1_sum_list[0.01 * _event_val_1_sum_list.size()];
    auto _val_1_max = _event_val_1_sum_list[0.99 * _event_val_1_sum_list.size()];
    auto _val_2_min = _event_val_2_sum_list[0.01 * _event_val_2_sum_list.size()];
    auto _val_2_max = _event_val_2_sum_list[0.99 * _event_val_2_sum_list.size()];

    auto bin_num_val_0 = (_val_0_max - _val_0_min) < bin_num_1d ? int(_val_0_max - _val_0_min) : bin_num_1d;
    auto bin_num_val_1 = (_val_1_max - _val_1_min) < bin_num_1d ? int(_val_1_max - _val_1_min) : bin_num_1d;
    auto bin_num_val_2 = (_val_2_max - _val_2_min) < bin_num_1d ? int(_val_2_max - _val_2_min) : bin_num_1d;

    auto hist_event_val_sum_0 = new TH1D("hist_event_val_sum_0", "hist_event_val_sum_0", bin_num_val_0, _val_0_min, _val_0_max);
    auto hist_event_val_sum_1 = new TH1D("hist_event_val_sum_1", "hist_event_val_sum_1", bin_num_val_1, _val_1_min, _val_1_max);
    auto hist_event_val_sum_2 = new TH1D("hist_event_val_sum_2", "hist_event_val_sum_2", bin_num_val_2, _val_2_min, _val_2_max);

    for (auto& _val : _event_val_0_sum_list){
        hist_event_val_sum_0->Fill(_val);
    }
    for (auto& _val : _event_val_1_sum_list){
        hist_event_val_sum_1->Fill(_val);
    }
    for (auto& _val : _event_val_2_sum_list){
        hist_event_val_sum_2->Fill(_val);
    }

    hist_event_val_sum_0->GetXaxis()->SetTitle("Event Val Sum");
    hist_event_val_sum_0->GetYaxis()->SetTitle("Count");
    hist_event_val_sum_0->SetFillColor(kBlue - 10);
    hist_event_val_sum_0->SetLineColor(kBlue);
    hist_event_val_sum_0->SetLineWidth(2);

    hist_event_val_sum_1->GetXaxis()->SetTitle("Event Val Sum");
    hist_event_val_sum_1->GetYaxis()->SetTitle("Count");
    hist_event_val_sum_1->SetFillColor(kBlue - 10);
    hist_event_val_sum_1->SetLineColor(kBlue);
    hist_event_val_sum_1->SetLineWidth(2);

    hist_event_val_sum_2->GetXaxis()->SetTitle("Event Val Sum");
    hist_event_val_sum_2->GetYaxis()->SetTitle("Count");
    hist_event_val_sum_2->SetFillColor(kBlue - 10);
    hist_event_val_sum_2->SetLineColor(kBlue);
    hist_event_val_sum_2->SetLineWidth(2);

    canvas_event_val_0_sum->cd();
    hist_event_val_sum_0->Draw("hist");
    canvas_event_val_0_sum->SetGrid();
    canvas_event_val_0_sum->Write();

    canvas_event_val_1_sum->cd();
    hist_event_val_sum_1->Draw("hist");
    canvas_event_val_1_sum->SetGrid();
    canvas_event_val_1_sum->Write();

    canvas_event_val_2_sum->cd();
    hist_event_val_sum_2->Draw("hist");
    canvas_event_val_2_sum->SetGrid();
    canvas_event_val_2_sum->Write();

    _event_val_0_sum_list.clear();
    _event_val_1_sum_list.clear();
    _event_val_2_sum_list.clear();
    canvas_event_val_0_sum->Close();
    canvas_event_val_1_sum->Close();
    canvas_event_val_2_sum->Close();

    // * --- Tp, Tc distribution --- *
    std::vector<uint8_t> _event_tp_list;
    std::vector<uint8_t> _event_tc_list;
    _event_tc_list.reserve(_frames.size()*76);
    _event_tp_list.reserve(_frames.size()*76);

    for (auto& _frame : _frames){
        for (auto& _chn : _frame._data){
            _event_tp_list.push_back(uint8_t(_chn.Tp));
            _event_tc_list.push_back(uint8_t(_chn.Tc));
        }
    }

    auto canvas_tptc = new TCanvas("canvas_tptc", "canvas_tptc", 800, 600);
    auto hist2d_tptc = new TH2I("hist2d_tptc", "hist2d_tptc", 2, 0, 2, 2, 0, 2);

    for (auto i = 0; i < _event_tp_list.size(); i++){
        hist2d_tptc->Fill(_event_tp_list[i], _event_tc_list[i]);
    }

    hist2d_tptc->GetXaxis()->SetTitle("Tp");
    hist2d_tptc->GetYaxis()->SetTitle("Tc");
    hist2d_tptc->SetStats(0);
    hist2d_tptc->Draw("colz");
    canvas_tptc->SetGrid();

    TLatex _latex;
    _latex.SetTextSize(0.05);
    _latex.SetTextAlign(22);
    _latex.SetTextColorAlpha(kRed, 0.5);
    _latex.DrawLatex(0.5, 0.55, "Case 1");
    _latex.DrawLatex(1.5, 0.55, "Case 2");
    _latex.DrawLatex(0.5, 1.55, "Case 3");
    _latex.DrawLatex(1.5, 1.55, "Case 4");

    _latex.SetTextSize(0.03);
    _latex.DrawLatex(0.5, 0.45, "TOT Not in Operation");
    _latex.DrawLatex(1.5, 0.45, "TOT Busy");
    _latex.DrawLatex(0.5, 1.45, "Most not Appear");
    _latex.DrawLatex(1.5, 1.45, "TOT Normal");

    canvas_tptc->Write();

    _event_tp_list.clear();
    _event_tc_list.clear();

    canvas_tptc->Close();

    output_root_file_ptr->Close();

    return 0;
}

void set_easylogger(){
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.setGlobally(el::ConfigurationType::Format, "%datetime{%H:%m:%s}[%levshort] (%fbase) %msg");
    defaultConf.set(el::Level::Info,    el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;34m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Warning, el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;33m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Error,   el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;31m%levshort\033[0m] (%fbase) %msg");
}