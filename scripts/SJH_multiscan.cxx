#include "easylogging++.h"
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TCanvas.h"
#include "TH2.h"
#include "TH1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TProfile.h"
#include "TRandom.h"
#include "TVector.h"
#include "SJH_frame.h"
#include <dirent.h>

INITIALIZE_EASYLOGGINGPP

void set_easylogger(); // set easylogging++ configurations

int main(int argc, char* argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    set_easylogger();

    auto folder_name_input  = "../dump/scan_analysis";
    auto file_name_output = "../dump/scan_analysis/Multiscan.root";

    int opt;
    while ((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt){
            case 'i':
                folder_name_input = optarg;
                break;
            case 'o':
                file_name_output = optarg;
                break;
            default:
                LOG(ERROR) << "Wrong arguments!";
                return 1;
        }
    }

    LOG(INFO) << "Input file folder: " << folder_name_input;
    LOG(INFO) << "Output file: " << file_name_output;

    // get file list
    std::string folder_name_input_str(folder_name_input);
    std::vector<std::string> file_name_list;
    std::vector<int> file_ch_list;

    // mapping from filename chn to real chn
    std::vector<int> mapping_file_chn = {
        0,   1,  2,  3,  4,  5,  6,  7,  8,  9,
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
        30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
        40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
        50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
        60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
        70, 71, 72
    };
    std::vector<int> actual_chn = {
        2,   3,  4,  5,  6,  7,  8,  9, 10, 11,
        12, 13, 14, 15, 16, 17, 18, 19, 21, 22,
        23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
        33, 34, 35, 36, 37, 38, 40, 41, 42, 43,
        44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
        54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
        64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
        74, 75, 76
    };
    std::map<int, int> mapping_file_chn_to_actual_chn;
    for (auto _index=0; _index < mapping_file_chn.size(); _index++){
        mapping_file_chn_to_actual_chn[mapping_file_chn[_index]] = actual_chn[_index];
    }

    auto folder_ptr = opendir(folder_name_input); // opendir is from dirent.h
    if (folder_ptr == NULL){
        LOG(ERROR) << "Cannot open input folder!";
        return 1;
    }
    struct dirent* entry_ptr;
    while ((entry_ptr = readdir(folder_ptr)) != NULL){
        std::string file_name(entry_ptr->d_name);
        if (file_name.find(".root") != std::string::npos){
            if (file_name.find("Ch") != std::string::npos){
                file_name_list.push_back(folder_name_input_str+"/"+file_name);
                auto num_len = file_name.find(".root") - file_name.find("Ch") - 2;
                auto ch_str = file_name.substr(file_name.find("Ch")+2, num_len);
                file_ch_list.push_back(std::stoi(ch_str));
                LOG(INFO) << "Find channel number: " << ch_str;
            }
            else{
                LOG(WARNING) << "Cannot find channel number in file name: " << file_name;
            }
        }
    }
    LOG(INFO) << "Total files: " << file_name_list.size();
    for (auto file_name : file_name_list){
        LOG(INFO) << file_name;
    }
    closedir(folder_ptr);

    // * -- list of  different files -- * //
    std::vector<std::vector<Double_t>> chn_values_list_list;
    std::vector<std::vector<Double_t>> times_list_list;
    std::vector<Int_t> ch_list;

    chn_values_list_list.reserve(file_name_list.size()*2);
    times_list_list.reserve(file_name_list.size()*2);
    ch_list.reserve(file_name_list.size()*2);

    for (auto _file_index=0; _file_index < file_name_list.size(); _file_index++){
        auto& _file_name = file_name_list[_file_index];
        auto& _file_chn = file_ch_list[_file_index];

        LOG(INFO) << "Processing file: " << _file_name;

        TFile* _file_ptr = new TFile(_file_name.c_str(), "READ");
        if (_file_ptr->IsZombie()){
            LOG(ERROR) << "Cannot open file: " << _file_name;
            continue;
        }

        _file_ptr->cd("Raw_val_0");

        int _coi_half_0 = mapping_file_chn_to_actual_chn[_file_chn];
        int _coi_half_1 = mapping_file_chn_to_actual_chn[_file_chn + 36];

        int _toi_half_0 = _coi_half_0 + 76;
        int _toi_half_1 = _coi_half_1 + 76;
    
        // Read the TVectorD from the index
        TVectorD* _vec_ptr_half_0 = (TVectorD*)gDirectory->Get(Form("TVectorT<double>;%d", _coi_half_0));
        TVectorD* _vec_ptr_half_1 = (TVectorD*)gDirectory->Get(Form("TVectorT<double>;%d", _coi_half_1));

        if (_vec_ptr_half_0 == NULL || _vec_ptr_half_1 == NULL){
            LOG(ERROR) << "Cannot find TVectorD in file: " << _file_name;
            continue;
        }

        auto _vec_half_0 = *_vec_ptr_half_0;
        auto _vec_half_1 = *_vec_ptr_half_1;

        // Read the TVectorD from the index
        TVectorD* _vec_ptr_toi_half_0 = (TVectorD*)gDirectory->Get(Form("TVectorT<double>;%d", _toi_half_0));
        TVectorD* _vec_ptr_toi_half_1 = (TVectorD*)gDirectory->Get(Form("TVectorT<double>;%d", _toi_half_1));

        if (_vec_ptr_toi_half_0 == NULL || _vec_ptr_toi_half_1 == NULL){
            LOG(ERROR) << "Cannot find TVectorD in file: " << _file_name;
            continue;
        }

        auto _vec_toi_half_0 = *_vec_ptr_toi_half_0;
        auto _vec_toi_half_1 = *_vec_ptr_toi_half_1;

        ch_list.push_back(_file_chn);
        ch_list.push_back(_file_chn + 38);

        std::vector<Double_t> _chn_values_listt_half_0;
        std::vector<Double_t> _chn_values_listt_half_1;

        std::vector<Double_t> _times_list_half_0;
        std::vector<Double_t> _times_list_half_1;

        for (auto _index=0; _index < _vec_half_0.GetNoElements(); _index++){
            _chn_values_listt_half_0.push_back(_vec_half_0[_index]);
            _chn_values_listt_half_1.push_back(_vec_half_1[_index]);

            _times_list_half_0.push_back(_vec_toi_half_0[_index]);
            _times_list_half_1.push_back(_vec_toi_half_1[_index]);
        }

        chn_values_list_list.push_back(_chn_values_listt_half_0);
        chn_values_list_list.push_back(_chn_values_listt_half_1);

        times_list_list.push_back(_times_list_half_0);
        times_list_list.push_back(_times_list_half_1);
    }

    // LOG(INFO) << "All channels: ";
    // for (auto _ch : ch_list){
    //     LOG(INFO) << _ch;
    // }

    // * -- calculate the mean and rms of each channel -- * //

    std::vector<std::vector<Double_t>> chn_mean_list_list;
    std::vector<std::vector<Double_t>> chn_rms_list_list;
    std::vector<std::vector<Double_t>> chn_mean_time_list_list;

    chn_mean_list_list.reserve(ch_list.size());
    chn_rms_list_list.reserve(ch_list.size());

    for (auto _index=0; _index < chn_values_list_list.size(); _index++){
        auto _chn_values_list = chn_values_list_list[_index];
        auto _times_list = times_list_list[_index];

        std::vector<Double_t> _chn_mean_list;
        std::vector<Double_t> _chn_rms_list;
        std::vector<Double_t> _chn_mean_time_list;

        // * -- calculate the mean and rms for the same time -- * //
        std::vector<Double_t> _chn_values_list_same_time;
        std::vector<Double_t> _chn_times_list_same_time;

        for (auto _index=0; _index < _chn_values_list.size(); _index++){
            auto _chn_value = _chn_values_list[_index];
            auto _time = _times_list[_index];

            if (_chn_times_list_same_time.size() == 0){
                _chn_times_list_same_time.push_back(_time);
                _chn_values_list_same_time.push_back(_chn_value);
            }
            else{
                if (_time == _chn_times_list_same_time.back()){
                    _chn_values_list_same_time.push_back(_chn_value);
                }
                else{
                    // calculate the mean and rms
                    auto _mean = 0.0;
                    auto _rms = 0.0;
                    for (auto _index=0; _index < _chn_values_list_same_time.size(); _index++){
                        _mean += _chn_values_list_same_time[_index];
                    }
                    _mean /= _chn_values_list_same_time.size();

                    for (auto _index=0; _index < _chn_values_list_same_time.size(); _index++){
                        _rms += (_chn_values_list_same_time[_index] - _mean)*(_chn_values_list_same_time[_index] - _mean);
                    }
                    _rms /= _chn_values_list_same_time.size();
                    _rms = sqrt(_rms);

                    _chn_mean_list.push_back(_mean);
                    _chn_rms_list.push_back(_rms);
                    _chn_mean_time_list.push_back(_chn_times_list_same_time.back());

                    _chn_times_list_same_time.clear();
                    _chn_values_list_same_time.clear();

                    _chn_times_list_same_time.push_back(_time);
                    _chn_values_list_same_time.push_back(_chn_value);
                }
            }
        }

        chn_mean_list_list.push_back(_chn_mean_list);
        chn_rms_list_list.push_back(_chn_rms_list);
        chn_mean_time_list_list.push_back(_chn_mean_time_list);

    }


    // * -- plot all channel plots in one canvas -- * //
    auto root_output_ptr = new TFile(file_name_output, "RECREATE");
    if (root_output_ptr->IsZombie()){
        LOG(ERROR) << "Cannot open output file: " << file_name_output;
        return 1;
    }
    auto canvas_multi_scan = new TCanvas("canvas_multi_scan", "canvas_multi_scan", 800, 600);
    int _row_num, _col_num;
    _row_num = 6;
    _col_num = 6;
    Double_t _uni_y_max = 260.0;
    Double_t _uni_y_min = 120.0;

    canvas_multi_scan->Divide(_col_num, _row_num);

    std::vector<int> plot_loc;
    // make loc based on ch_list value order from 1 to ch_list.size()
    auto sorted_ch_list = ch_list;
    std::sort(sorted_ch_list.begin(), sorted_ch_list.end());
    for (auto _ch : ch_list){
        auto _index = std::find(sorted_ch_list.begin(), sorted_ch_list.end(), _ch) - sorted_ch_list.begin();
        plot_loc.push_back(_index+1);
    }

    for (int i = 1; i <= _col_num * _row_num; ++i) {
        canvas_multi_scan->cd(i);
        gPad->SetTopMargin(0.00);
        gPad->SetBottomMargin(0.00);
        gPad->SetLeftMargin(0.00);
        gPad->SetRightMargin(0.00);
    }   

    for (auto _index=0; _index < chn_values_list_list.size(); _index++){
        auto _chn_values_list = chn_values_list_list[_index];
        auto _times_list = times_list_list[_index];
        auto _ch = ch_list[_index];

        canvas_multi_scan->cd(plot_loc[_index]);
        auto graph_scan = new TGraphErrors();
        graph_scan->SetName(Form("graph_scan_%d", _ch));
        graph_scan->SetTitle(Form("Channel %d", _ch));
        graph_scan->SetMarkerStyle(20);
        graph_scan->SetMarkerSize(0.5);
        graph_scan->SetMarkerColor(kBlue);

        for (auto _time_index=0; _time_index < chn_mean_time_list_list[_index].size(); _time_index++){
            auto _time = chn_mean_time_list_list[_index][_time_index];
            auto _mean = chn_mean_list_list[_index][_time_index];
            auto _rms = chn_rms_list_list[_index][_time_index];

            graph_scan->SetPoint(_time_index, _time, _mean);
            graph_scan->SetPointError(_time_index, 0, _rms);
        }

        // ignore axis label

        graph_scan->GetYaxis()->SetRangeUser(_uni_y_min, _uni_y_max);
        graph_scan->Draw("AP");
    }

    canvas_multi_scan->Write();

    
    root_output_ptr->Close();
    return 0;
}

void set_easylogger(){
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.setGlobally(el::ConfigurationType::Format, "%datetime{%H:%m:%s}[%levshort] (%fbase) %msg");
    defaultConf.set(el::Level::Info,    el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;34m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Warning, el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;33m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Error,   el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;31m%levshort\033[0m] (%fbase) %msg");
}