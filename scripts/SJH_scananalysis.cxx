#include "easylogging++.h"
#include "TFile.h"
#include "TTree.h"
#include "TClonesArray.h"
#include "TCanvas.h"
#include "TH2.h"
#include "TH1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TProfile.h"
#include "TRandom.h"
#include "TVector.h"
#include "TPaveText.h"
#include "TPaletteAxis.h"
#include "SJH_frame.h"
#include <dirent.h>

INITIALIZE_EASYLOGGINGPP

void set_easylogger(); // set easylogging++ configurations

int main(int argc, char* argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    set_easylogger();

    auto folder_name_input  = "../dump/rootifier/Scan_20231013_001";
    auto file_name_output = "../dump/scan_analysis/Scan_20231013_001.root";

    int opt;
    while ((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt){
            case 'i':
                folder_name_input = optarg;
                break;
            case 'o':
                file_name_output = optarg;
                break;
            default:
                LOG(ERROR) << "Wrong arguments!";
                return 1;
        }
    }

    LOG(INFO) << "Input file folder: " << folder_name_input;
    LOG(INFO) << "Output file: " << file_name_output;
    std::string file_name_input_str(folder_name_input);
    std::string scan_info_str = file_name_input_str.substr(file_name_input_str.find("Scan_"), file_name_input_str.length() - file_name_input_str.find("Scan_"));
    
    Double_t delay_unit = 25; // ns
    Double_t phase_unit = 1.5625 ; // 25 /16 ns

    // get list of files in the folder
    std::string folder_name_input_str(folder_name_input);
    std::vector<std::string> file_name_list;
    std::vector<int> file_delay_list;
    std::vector<int> file_phase_list;
    auto folder_ptr = opendir(folder_name_input); // opendir is from dirent.h
    if (folder_ptr == NULL){
        LOG(ERROR) << "Cannot open input folder!";
        return 1;
    }
    struct dirent* entry_ptr;
    while ((entry_ptr = readdir(folder_ptr)) != NULL){
        std::string file_name(entry_ptr->d_name);
        if (file_name.find(".root") != std::string::npos){
            file_name_list.push_back(folder_name_input_str + "/" + file_name);
            auto delay_word = std::string("Delay");
            auto delay_pos = file_name.find(delay_word);
            auto delay_word_len = delay_word.length();
            auto phase_word = std::string("Phase");
            auto phase_pos = file_name.find(phase_word);
            auto phase_word_len = phase_word.length();
            std::string phase_str; 
            if (phase_pos == std::string::npos){
                LOG(ERROR) << "Cannot find phase in file name: " << file_name;
                phase_str = "0";
                phase_pos = file_name.find(".root");
            } else {
                phase_str = file_name.substr(phase_pos + phase_word_len, file_name.find(".root") - phase_pos - phase_word_len);
            }
            auto delay_str = file_name.substr(delay_pos + delay_word_len, phase_pos - delay_pos - delay_word_len);
            LOG(INFO) << "Delay: " << delay_str << " Phase: " << phase_str;
            file_delay_list.push_back(std::stoi(delay_str));
            file_phase_list.push_back(std::stoi(phase_str));
        }
    }
    LOG(INFO) << "Total files: " << file_name_list.size();
    for (auto file_name : file_name_list){
        LOG(INFO) << file_name;
    }
    closedir(folder_ptr);

    // * --- start data analysis --- *
    std::vector<TFile*> file_ptr_list;
    file_ptr_list.reserve(file_name_list.size());

    for (auto file_name: file_name_list){
        auto file_ptr = new TFile(file_name.c_str(), "READ");
        if (!file_ptr->IsOpen()){
            LOG(ERROR) << "Cannot open file: " << file_name;
            return 1;
        }
        file_ptr_list.push_back(file_ptr);
    }

    std::vector<std::vector<HGCROC_Frame>> frame_list_list;
    frame_list_list.reserve(file_ptr_list.size());

    for (auto file_ptr: file_ptr_list){
        auto tree_ptr = (TTree*)file_ptr->Get("HF_T");
        if (tree_ptr == nullptr){
            LOG(ERROR) << "Cannot find tree in file: " << file_ptr->GetName();
            return 1;
        }

        HGCROC_Frame* frame_ptr = nullptr;
        tree_ptr->SetBranchAddress("HGCROC_Frames", &frame_ptr);

        std::vector<HGCROC_Frame> frame_list;
        frame_list.reserve(tree_ptr->GetEntries());

        for (int i = 0; i < tree_ptr->GetEntries(); i++){
            tree_ptr->GetEntry(i);
            frame_list.push_back(*frame_ptr);
        }

        frame_list_list.push_back(frame_list);
    }

    for (auto root_ptr: file_ptr_list){
        root_ptr->Close();
    }

    // * --- start plotting --- *
    auto output_file_ptr = new TFile(file_name_output, "RECREATE");

    std::vector<std::vector<Double_t>> channel_mean_list;
    std::vector<std::vector<Double_t>> channel_err_list;
    channel_mean_list.resize(76);
    channel_err_list.resize(76);

    std::vector<std::vector<uint16_t>> channel_val_list_list;
    channel_val_list_list.reserve(76);

    std::vector<std::vector<Double_t>> channel_val_vec_list;
    channel_val_vec_list.resize(76);

    std::vector<std::vector<Double_t>> time_val_vec_list;
    time_val_vec_list.resize(76);

    std::vector<std::vector<std::vector<uint16_t>>> channel_val_list_list_list;

    for (size_t i = 0; i < frame_list_list.size(); ++i) {
        auto& frame_list = frame_list_list[i];
        auto& delay = file_delay_list[i];
        auto& phase = file_phase_list[i];
        channel_val_list_list.clear();
        channel_val_list_list.resize(76);
        for (auto frame: frame_list){
            if (frame.get_DaqH_H1(0) != 0 || frame.get_DaqH_H2(0) != 0 || frame.get_DaqH_H3(0) != 0)
                continue;
            if (frame.get_DaqH_H1(1) != 0 || frame.get_DaqH_H2(1) != 0 || frame.get_DaqH_H3(1) != 0)
                continue;
            for (int i = 0; i < 76; i++){
                auto _val = frame._data[i].val_10b_0;
                channel_val_list_list[i].push_back(_val);
                channel_val_vec_list[i].push_back(Double_t(_val));
                time_val_vec_list[i].push_back(Double_t(delay*delay_unit + phase*phase_unit));
            }
        }
        channel_val_list_list_list.push_back(channel_val_list_list);
        
        Double_t _mean = 0;
        Double_t _err = 0;

        for (int i = 0; i < 76; i++){
            _mean = 0;
            _err = 0;
            for (auto val: channel_val_list_list[i]){
                _mean += val;
            }
            _mean /= channel_val_list_list[i].size();
            for (auto val: channel_val_list_list[i]){
                _err += (val - _mean) * (val - _mean);
            }
            _err = std::sqrt(_err / channel_val_list_list[i].size());
            channel_mean_list[i].push_back(_mean);
            channel_err_list[i].push_back(_err);
        }
    }

    output_file_ptr->mkdir("Raw_val_0");
    output_file_ptr->cd("Raw_val_0");

    for (auto _chn_val_vec: channel_val_vec_list){
        TVectorD _chn_val_vec_t(_chn_val_vec.size(), _chn_val_vec.data());
        _chn_val_vec_t.Write();
    }

    for (auto _time_val_vec: time_val_vec_list){
        TVectorD _time_val_vec_t(_time_val_vec.size(), _time_val_vec.data());
        _time_val_vec_t.Write();
    }
    output_file_ptr->mkdir("Channel_val_0_Delay_Scan");
    output_file_ptr->cd("Channel_val_0_Delay_Scan");
    // use delay list as x axis, channel mean as y axis, channel err as error

    for (int i = 0; i < 76; i++){
        auto canvas_chn = new TCanvas(("Channel_" + std::to_string(i)).c_str(), ("Channel_" + std::to_string(i)).c_str(), 800, 600);
        auto graph_chn = new TGraphErrors();
        std::vector<Double_t> time_ns_array;
        for (int j = 0; j < file_delay_list.size(); j++){
            time_ns_array.push_back(file_delay_list[j]*delay_unit + file_phase_list[j]*phase_unit);
        }
        std::sort(time_ns_array.begin(), time_ns_array.end());
        auto _bin_num = int((time_ns_array[time_ns_array.size()-1] - time_ns_array[0])/phase_unit);
        auto hist2d_chn = new TH2D(("Channel_" + std::to_string(i)).c_str(), ("Channel_" + std::to_string(i)).c_str(), _bin_num , time_ns_array[0], time_ns_array[time_ns_array.size()-1], 1024, 0, 1024);
        // std::vector<Double_t> time_ns_array;
        Double_t _val_max = -1;
        graph_chn->SetTitle(("Channel_" + std::to_string(i)).c_str());
        graph_chn->SetMarkerStyle(20);
        graph_chn->SetMarkerSize(1);
        graph_chn->SetMarkerColor(kBlue);
        graph_chn->SetLineColor(kBlue);

        for (int j = 0; j < file_delay_list.size(); j++){
            Double_t _time_ns = file_delay_list[j]*delay_unit + file_phase_list[j]*phase_unit;
            for (auto val: channel_val_list_list_list[j][i]){
                hist2d_chn->Fill(_time_ns, val);
            }
            graph_chn->SetPoint(j, _time_ns, channel_mean_list[i][j]);
            time_ns_array.push_back(_time_ns);
            if (channel_mean_list[i][j] + channel_err_list[i][j] > _val_max){
                _val_max = channel_mean_list[i][j] + channel_err_list[i][j];
            }
            graph_chn->SetPointError(j, 0, channel_err_list[i][j]);
        }

        graph_chn->GetXaxis()->SetTitle("Delay [ns]");
        graph_chn->GetYaxis()->SetTitle("Channel value [ADC]");

        std::sort(time_ns_array.begin(), time_ns_array.end());
        auto _time_ns_min = time_ns_array[int(0.05 * time_ns_array.size())];
        auto _time_ns_max = time_ns_array[int(0.95 * time_ns_array.size())];
        auto _time_ns_range = _time_ns_max - _time_ns_min;
        _time_ns_max += 0.05 * _time_ns_range;
        _time_ns_min -= 0.05 * _time_ns_range;
        graph_chn->GetXaxis()->SetRangeUser(_time_ns_min, _time_ns_max);

        graph_chn->GetYaxis()->SetRangeUser(0, _val_max * 1.1);

        graph_chn->Draw("AP");

        // canvas_chn->SetGrid();
        canvas_chn->SetGridx();
        canvas_chn->Write();
        canvas_chn->Close();

        auto canvas_hist2d = new TCanvas(("Channel_" + std::to_string(i) + "_2D").c_str(), ("Channel_" + std::to_string(i) + "_2D").c_str(), 1200, 900);

        hist2d_chn->GetXaxis()->SetTitle("Delay [ns]");
        hist2d_chn->GetYaxis()->SetTitle("Channel value [ADC]");

        hist2d_chn->Draw("COLZ");
        // hist2d_chn->GetYaxis()->SetRangeUser(0, 1024);
        hist2d_chn->GetYaxis()->SetRangeUser(170, 260);
        hist2d_chn->SetStats(0);
        hist2d_chn->SetTitle("");
        // increate right margin to fit the color bar
        canvas_hist2d->SetRightMargin(0.13);    

        std::string _entry_num_str = "Entry: " + std::to_string(hist2d_chn->GetEntries());
        std::string _mean_x_str = "Mean X: " + std::to_string(hist2d_chn->GetMean(1));
        std::string _mean_y_str = "Mean Y: " + std::to_string(hist2d_chn->GetMean(2));
        std::string _rms_x_str = "RMS X: " + std::to_string(hist2d_chn->GetRMS(1));
        std::string _rms_y_str = "RMS Y: " + std::to_string(hist2d_chn->GetRMS(2));

        TPaveText pt(0.67, 0.6, 0.87, 0.9, "NDC");
        pt.SetFillColor(0);
        pt.SetBorderSize(3);
        pt.SetTextAlign(12);
        pt.SetTextFont(42);
        pt.SetTextSize(0.03);

        pt.AddText(_entry_num_str.c_str());
        pt.AddText(_mean_x_str.c_str());
        pt.AddText(_mean_y_str.c_str());
        pt.AddText(_rms_x_str.c_str());
        pt.AddText(_rms_y_str.c_str());

        // pt.Draw();

        int real_chn_num = i;
        if (i > 0)
            real_chn_num -= 1;
        if (i > 20)
            real_chn_num -= 1;
        if (i > 36)
            real_chn_num -= 1;
        if (i > 56)
            real_chn_num -= 1;

        TLatex latex;
        latex.SetTextSize(0.04);
        latex.SetTextAlign(31);
        latex.SetTextFont(62);
        latex.DrawLatexNDC(0.86, 0.85, "H2GCROC Readout Test");
        latex.SetTextSize(0.035);
        latex.SetTextFont(42);
        // latex.DrawLatexNDC(0.85, 0.80, ("Channel " + std::to_string(real_chn_num) + ", PS Beam Test").c_str());
        // latex.DrawLatexNDC(0.85, 0.75, "Oct. 2023");
        // latex.DrawLatexNDC(0.85, 0.75, ("Channel " + std::to_string(real_chn_num) + ", 56.5 Vbias").c_str());
        // latex.DrawLatexNDC(0.85, 0.80, "CERN PS Beam Test, 10 GeV pi-");
        // latex.DrawLatexNDC(0.85, 0.70, "October 2023");
        latex.DrawLatexNDC(0.85, 0.80, "Channel 2, Internal Injection");
        latex.DrawLatexNDC(0.85, 0.75, "October 2023");

        // move the color bar
        // canvas_hist2d->SetGrid();
        canvas_hist2d->Update();
        auto palette = (TPaletteAxis*)hist2d_chn->GetListOfFunctions()->FindObject("palette");
        palette->SetX1NDC(0.88);


        canvas_hist2d->SetLogz();

        // if the folder does not exist, create it
        auto folder_name = std::string("dump/pics/") + scan_info_str;
        auto folder_ptr = opendir(folder_name.c_str());
        if (folder_ptr == NULL){
            LOG(INFO) << "Creating folder: " << folder_name;
            auto mkdir_status = mkdir(folder_name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            if (mkdir_status == -1){
                LOG(ERROR) << "Cannot create folder: " << folder_name;
                return 1;
            }
        }

        canvas_hist2d->SaveAs(("dump/pics/" + scan_info_str + "/Channel_" + std::to_string(i) + "_2D.png").c_str());
        canvas_hist2d->Close();
    }

    output_file_ptr->cd();
    

    channel_mean_list.clear();
    channel_err_list.clear();
    channel_mean_list.resize(76);
    channel_err_list.resize(76);

    channel_val_list_list.clear();
    channel_val_list_list.resize(76);
    time_val_vec_list.clear();
    time_val_vec_list.resize(76);

    for (size_t i = 0; i < frame_list_list.size(); ++i) {
        auto& frame_list = frame_list_list[i];
        auto& delay = file_delay_list[i];
        auto& phase = file_phase_list[i];
        channel_val_list_list.clear();
        channel_val_list_list.resize(76);
        for (auto frame: frame_list){
            if (frame.get_DaqH_H1(0) != 0 || frame.get_DaqH_H2(0) != 0 || frame.get_DaqH_H3(0) != 0)
                continue;
            if (frame.get_DaqH_H1(1) != 0 || frame.get_DaqH_H2(1) != 0 || frame.get_DaqH_H3(1) != 0)
                continue;
            for (int i = 0; i < 76; i++){
                auto _val = frame._data[i].val_10b_1;
                channel_val_list_list[i].push_back(_val);
                channel_val_vec_list[i].push_back(Double_t(_val));
                time_val_vec_list[i].push_back(Double_t(delay*delay_unit + phase*phase_unit));
            }
        }
        
        Double_t _mean = 0;
        Double_t _err = 0;

        for (int i = 0; i < 76; i++){
            _mean = 0;
            _err = 0;
            for (auto val: channel_val_list_list[i]){
                _mean += val;
            }
            _mean /= channel_val_list_list[i].size();
            for (auto val: channel_val_list_list[i]){
                _err += (val - _mean) * (val - _mean);
            }
            _err = std::sqrt(_err / channel_val_list_list[i].size());
            channel_mean_list[i].push_back(_mean);
            channel_err_list[i].push_back(_err);
        }
    }

    output_file_ptr->mkdir("Raw_val_1");
    output_file_ptr->cd("Raw_val_1");

    for (auto _chn_val_vec: channel_val_vec_list){
        TVectorD _chn_val_vec_t(_chn_val_vec.size(), _chn_val_vec.data());
        _chn_val_vec_t.Write();
    }

    for (auto _time_val_vec: time_val_vec_list){
        TVectorD _time_val_vec_t(_time_val_vec.size(), _time_val_vec.data());
        _time_val_vec_t.Write();
    }

    // use phase list as x axis, channel mean as y axis, channel err as error
    output_file_ptr->mkdir("Channel_val_1_Delay_Scan");
    output_file_ptr->cd("Channel_val_1_Delay_Scan");

    for (int i = 0; i < 76; i++){
        auto canvas_chn = new TCanvas(("Channel_" + std::to_string(i)).c_str(), ("Channel_" + std::to_string(i)).c_str(), 800, 600);
        auto graph_chn = new TGraphErrors();
        std::vector<Double_t> time_ns_array;
        Double_t _val_max = -1;
        graph_chn->SetTitle(("Channel_" + std::to_string(i)).c_str());
        graph_chn->SetMarkerStyle(20);
        graph_chn->SetMarkerSize(1);
        graph_chn->SetMarkerColor(kBlue);
        graph_chn->SetLineColor(kBlue);

        for (int j = 0; j < file_delay_list.size(); j++){
            Double_t _time_ns = file_delay_list[j]*delay_unit + file_phase_list[j]*phase_unit;
            graph_chn->SetPoint(j, _time_ns, channel_mean_list[i][j]);
            time_ns_array.push_back(_time_ns);
            if (channel_mean_list[i][j] + channel_err_list[i][j] > _val_max){
                _val_max = channel_mean_list[i][j] + channel_err_list[i][j];
            }
            graph_chn->SetPointError(j, 0, channel_err_list[i][j]);
        }

        graph_chn->GetXaxis()->SetTitle("Delay [ns]");
        graph_chn->GetYaxis()->SetTitle("Channel value [ADC]");

        std::sort(time_ns_array.begin(), time_ns_array.end());
        auto _time_ns_min = time_ns_array[int(0.05 * time_ns_array.size())];
        auto _time_ns_max = time_ns_array[int(0.95 * time_ns_array.size())];
        auto _time_ns_range = _time_ns_max - _time_ns_min;
        _time_ns_max += 0.05 * _time_ns_range;
        _time_ns_min -= 0.05 * _time_ns_range;
        graph_chn->GetXaxis()->SetRangeUser(_time_ns_min, _time_ns_max);

        graph_chn->GetYaxis()->SetRangeUser(0, _val_max * 1.1);

        graph_chn->Draw("AP");
        // canvas_chn->SetGrid();
        // canvas_chn->SetGridx();
        canvas_chn->Write();
        canvas_chn->Close();
    }

    output_file_ptr->cd();

    channel_mean_list.clear();
    channel_err_list.clear();
    channel_mean_list.resize(76);
    channel_err_list.resize(76);

    channel_val_list_list.clear();
    channel_val_list_list.resize(76);
    time_val_vec_list.clear();
    time_val_vec_list.resize(76);

    for (size_t i = 0; i < frame_list_list.size(); ++i) {
        auto& frame_list = frame_list_list[i];
        auto& delay = file_delay_list[i];
        auto& phase = file_phase_list[i];
        channel_val_list_list.clear();
        channel_val_list_list.resize(76);
        for (auto frame: frame_list){
            if (frame.get_DaqH_H1(0) != 0 || frame.get_DaqH_H2(0) != 0 || frame.get_DaqH_H3(0) != 0)
                continue;
            if (frame.get_DaqH_H1(1) != 0 || frame.get_DaqH_H2(1) != 0 || frame.get_DaqH_H3(1) != 0)
                continue;
            for (int i = 0; i < 76; i++){
                auto _val = frame._data[i].val_10b_2;
                channel_val_list_list[i].push_back(_val);
                channel_val_vec_list[i].push_back(Double_t(_val));
                time_val_vec_list[i].push_back(Double_t(delay*delay_unit + phase*phase_unit));
            }
        }
        
        Double_t _mean = 0;
        Double_t _err = 0;

        for (int i = 0; i < 76; i++){
            _mean = 0;
            _err = 0;
            for (auto val: channel_val_list_list[i]){
                _mean += val;
            }
            _mean /= channel_val_list_list[i].size();
            for (auto val: channel_val_list_list[i]){
                _err += (val - _mean) * (val - _mean);
            }
            _err = std::sqrt(_err / channel_val_list_list[i].size());
            channel_mean_list[i].push_back(_mean);
            channel_err_list[i].push_back(_err);
        }
    }

    output_file_ptr->mkdir("Raw_val_2");
    output_file_ptr->cd("Raw_val_2");

    for (auto _chn_val_vec: channel_val_vec_list){
        TVectorD _chn_val_vec_t(_chn_val_vec.size(), _chn_val_vec.data());
        _chn_val_vec_t.Write();
    }

    for (auto _time_val_vec: time_val_vec_list){
        TVectorD _time_val_vec_t(_time_val_vec.size(), _time_val_vec.data());
        _time_val_vec_t.Write();
    }
    
    // use delay list as x axis, channel mean as y axis, channel err as error
    output_file_ptr->mkdir("Channel_val_2_Delay_Scan");
    output_file_ptr->cd("Channel_val_2_Delay_Scan");

    for (int i = 0; i < 76; i++){
        auto canvas_chn = new TCanvas(("Channel_" + std::to_string(i)).c_str(), ("Channel_" + std::to_string(i)).c_str(), 800, 600);
        auto graph_chn = new TGraphErrors();
        std::vector<Double_t> time_ns_array;
        Double_t _val_max = -1;
        graph_chn->SetTitle(("Channel_" + std::to_string(i)).c_str());
        graph_chn->SetMarkerStyle(20);
        graph_chn->SetMarkerSize(1);
        graph_chn->SetMarkerColor(kBlue);
        graph_chn->SetLineColor(kBlue);

        for (int j = 0; j < file_delay_list.size(); j++){
            Double_t _time_ns = file_delay_list[j]*delay_unit + file_phase_list[j]*phase_unit;
            graph_chn->SetPoint(j, _time_ns, channel_mean_list[i][j]);
            time_ns_array.push_back(_time_ns);
            if (channel_mean_list[i][j] + channel_err_list[i][j] > _val_max){
                _val_max = channel_mean_list[i][j] + channel_err_list[i][j];
            }
            graph_chn->SetPointError(j, 0, channel_err_list[i][j]);
        }

        graph_chn->GetXaxis()->SetTitle("Delay [ns]");
        graph_chn->GetYaxis()->SetTitle("Channel value [ADC]");

        std::sort(time_ns_array.begin(), time_ns_array.end());
        auto _time_ns_min = time_ns_array[int(0.05 * time_ns_array.size())];
        auto _time_ns_max = time_ns_array[int(0.95 * time_ns_array.size())];
        auto _time_ns_range = _time_ns_max - _time_ns_min;
        _time_ns_max += 0.05 * _time_ns_range;
        _time_ns_min -= 0.05 * _time_ns_range;
        graph_chn->GetXaxis()->SetRangeUser(_time_ns_min, _time_ns_max);

        graph_chn->GetYaxis()->SetRangeUser(0, _val_max * 1.1);

        graph_chn->Draw("AP");
        // canvas_chn->SetGrid();
        canvas_chn->SetGridx();
        canvas_chn->Write();
        canvas_chn->Close();
    }

    output_file_ptr->Close();

    return 0;
}

void set_easylogger(){
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.setGlobally(el::ConfigurationType::Format, "%datetime{%H:%m:%s}[%levshort] (%fbase) %msg");
    defaultConf.set(el::Level::Info,    el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;34m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Warning, el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;33m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Error,   el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;31m%levshort\033[0m] (%fbase) %msg");
}