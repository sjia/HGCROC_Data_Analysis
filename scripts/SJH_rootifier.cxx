#include "easylogging++.h"
#include "TFile.h"
#include "TTree.h"
#include "SJH_frame.h"

INITIALIZE_EASYLOGGINGPP

void set_easylogger(); // set easylogging++ configurations

int main(int argc, char* argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    set_easylogger();

    auto file_name_input  = "data/TestLog_20231012_090906_172.txt";
    auto file_name_output = "dump/data_20231012_090906_172.root";

    int opt;
    while ((opt = getopt(argc, argv, "i:o:")) != -1){
        switch (opt){
            case 'i':
                file_name_input = optarg;
                break;
            case 'o':
                file_name_output = optarg;
                break;
            default:
                LOG(ERROR) << "Wrong arguments!";
                return 1;
        }
    }

    LOG(INFO) << "Input file: " << file_name_input;
    LOG(INFO) << "Output file: " << file_name_output;

    std::string file_name_input_str(file_name_input);
    if (file_name_input_str.find(".txt") == std::string::npos){
        LOG(ERROR) << "Input file should be a .txt file!";
        return 1;
    }

    auto txt_file_ptr = std::fstream(file_name_input, std::ios::in);
    if (!txt_file_ptr.is_open()){
        LOG(ERROR) << "Cannot open input file!";
        return 1;
    }

    int line_num = 0;
    std::string _line;
    while (std::getline(txt_file_ptr, _line)){
        line_num++;
    }

    // reset file pointer
    txt_file_ptr.clear();
    txt_file_ptr.seekg(0, std::ios::beg);

    LOG(INFO) << "Total lines: " << line_num;

    uint32_t _event_id = -1;
    std::vector<Byte_t> _event_bytes_array;
    _event_bytes_array.reserve(400);

    auto root_file_ptr = new TFile(file_name_output, "RECREATE");

    LOG(INFO) << "Current directory: " << gDirectory->GetName();
    auto tree_ptr = new TTree("HF_T", "HF_T");
    HGCROC_Frame* _frame_ptr = nullptr;
    auto _branch_ptr = tree_ptr->Branch("HGCROC_Frames", "HGCROC_Frame", &_frame_ptr);
    LOG(INFO) << "Branch name: " << _branch_ptr->GetName();
    for (int i = 0; i < line_num; i++){
        std::getline(txt_file_ptr, _line);
        if (_line.find("Config :") != std::string::npos){
            // it's a config line
            // LOG(INFO) << _line;
            continue;
        }
        if (_line.find(file_name_input_str) != std::string::npos){
            // it's a file name line
            // LOG(INFO) << _line;
            continue;
        }
        int _intro_pos = _line.find("Bytes (Rx): ");
        if (_intro_pos != std::string::npos){
            _line = _line.substr(_intro_pos + 12);
        }
        // LOG(INFO) << _line;

        std::vector<Byte_t> _bytes_array;
        _bytes_array.reserve(40);
        // split string by space
        std::istringstream _iss(_line);
        std::string _byte_str;
        while (std::getline(_iss, _byte_str, ' ')){
           _bytes_array.push_back(std::strtol(_byte_str.c_str(), nullptr, 16));
        }
        // LOG(INFO) << "Bytes array size: " << _bytes_array.size();
        
        uint32_t _current_event_id = (_bytes_array[4] << 24) + 
                                     (_bytes_array[5] << 16) + 
                                     (_bytes_array[6] << 8) + 
                                     (_bytes_array[7]);
        if (_event_id == -1){
            _event_id = _current_event_id;
            _event_bytes_array.insert(_event_bytes_array.end(), _bytes_array.begin(), _bytes_array.end());
        } else {
            if (_event_id != _current_event_id){
                _event_id = _current_event_id;
                if (_event_bytes_array.size() != 400){
                    LOG(ERROR) << "Event bytes array size is not 400!";
                } else {
                    _frame_ptr = new HGCROC_Frame(&_event_bytes_array);
                    tree_ptr->Fill();
                    // delete _frame_ptr;
                }
                _event_bytes_array.clear();
                _event_bytes_array.insert(_event_bytes_array.end(), _bytes_array.begin(), _bytes_array.end());
            } else {
                _event_bytes_array.insert(_event_bytes_array.end(), _bytes_array.begin(), _bytes_array.end());
            }
        }
    }
    
    tree_ptr->Write();
    txt_file_ptr.close();
    
    root_file_ptr->Close();

    return 0;
}

void set_easylogger(){
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.setGlobally(el::ConfigurationType::Format, "%datetime{%H:%m:%s}[%levshort] (%fbase) %msg");
    defaultConf.set(el::Level::Info,    el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;34m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Warning, el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;33m%levshort\033[0m] (%fbase) %msg");
    defaultConf.set(el::Level::Error,   el::ConfigurationType::Format, 
        "%datetime{%H:%m:%s}[\033[1;31m%levshort\033[0m] (%fbase) %msg");
}