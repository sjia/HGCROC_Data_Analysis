#include "SJH_frame.h"

HGCROC_Frame::HGCROC_Frame(std::vector<Byte_t>*_raw_data):
    TObject(){
    // 1. check the size of raw data
    if (_raw_data->size() != 400){
        LOG(ERROR) << "Raw data size is not 16 bytes!";
        return;
    }

    Byte_t _chip_id   = 0x00;
    Byte_t _half_id   = 0x00;
    std::vector<Byte_t> _line_number_vec;
    _line_number_vec.reserve(10);
    std::vector<uint32_t> _eventid_vec;
    _eventid_vec.reserve(10);
    std::vector<HGCROC_CHN_Data> _data_half_0;
    _data_half_0.reserve(38);

    for (auto _line_index=0; _line_index<5; _line_index++){
        int _start_index = _line_index * 40;
        if (_line_index==0) {
            _chip_id = _raw_data->at(_start_index);
            _half_id = _raw_data->at(_start_index+1);
        }
        else {
            if (_chip_id != _raw_data->at(_start_index)){
                LOG(ERROR) << "Chip ID is not consistent!";
                return;
            }
            if (_half_id != _raw_data->at(_start_index+1)){
                LOG(ERROR) << "Half ID is not consistent!";
                return;
            }
        }
        _line_number_vec.push_back(_raw_data->at(_start_index+3));
        _eventid_vec.push_back(
            (_raw_data->at(_start_index+4) << 24) + 
            (_raw_data->at(_start_index+5) << 16) + 
            (_raw_data->at(_start_index+6) << 8) + 
            (_raw_data->at(_start_index+7))
        );
        if (_line_index==0) {
            this->DaqH_0 = (_raw_data->at(_start_index+8) << 24) + 
                         (_raw_data->at(_start_index+9) << 16) + 
                         (_raw_data->at(_start_index+10) << 8) + 
                         (_raw_data->at(_start_index+11));
        } else {
            _data_half_0.push_back(get_chn_data(_raw_data->at(_start_index+8), _raw_data->at(_start_index+9), _raw_data->at(_start_index+10), _raw_data->at(_start_index+11)));
        }
        for (auto _inline_index=3; _inline_index<9; _inline_index++) {
            _data_half_0.push_back(get_chn_data(_raw_data->at(_start_index+(_inline_index*4)), _raw_data->at(_start_index+(_inline_index*4)+1), _raw_data->at(_start_index+(_inline_index*4)+2), _raw_data->at(_start_index+(_inline_index*4)+3)));
        }
        if (_line_index==4){
            this->CRC_32_0 = (
                (_raw_data->at(_start_index+36) << 24) + 
                (_raw_data->at(_start_index+37) << 16) + 
                (_raw_data->at(_start_index+38) << 8) + 
                (_raw_data->at(_start_index+39))
            );
        } else {
            _data_half_0.push_back(get_chn_data(_raw_data->at(_start_index+36), _raw_data->at(_start_index+37), _raw_data->at(_start_index+38), _raw_data->at(_start_index+39)));
        }

    }

    // LOG(INFO) << "Chip ID: " << std::hex << (int)_chip_id;
    // LOG(INFO) << "Half ID: " << std::hex << (int)_half_id;
    // LOG(INFO) << "Line number: " << std::hex << (int)_line_number_vec.at(0) << " " << std::hex << (int)_line_number_vec.at(1) << " " << std::hex << (int)_line_number_vec.at(2) << " " << std::hex << (int)_line_number_vec.at(3) << " " << std::hex << (int)_line_number_vec.at(4);
    // LOG(INFO) << "CRC: " << std::hex << CRC_32_0;
    // LOG(INFO) << "Event ID: " << std::dec << _eventid_vec.at(0) << " " << std::dec << _eventid_vec.at(1) << " " << std::dec << _eventid_vec.at(2) << " " << std::dec << _eventid_vec.at(3) << " " << std::dec << _eventid_vec.at(4);
    // LOG(INFO) << "DAQH: " << std::hex << this->DaqH_0;
    // LOG(INFO) << "DaqH Hd: " << std::dec << (int)this->get_DaqH_Hd(0);
    // LOG(INFO) << "DaqH BC: " << std::dec << (int)this->get_DaqH_BC(0);
    // LOG(INFO) << "DaqH EC: " << std::dec << (int)this->get_DaqH_EC(0);
    // LOG(INFO) << "DaqH OB: " << std::dec << (int)this->get_DaqH_OB(0);
    // LOG(INFO) << "DaqH H1: " << std::dec << (int)this->get_DaqH_H1(0);
    // LOG(INFO) << "DaqH H2: " << std::dec << (int)this->get_DaqH_H2(0);
    // LOG(INFO) << "DaqH H3: " << std::dec << (int)this->get_DaqH_H3(0);
    // LOG(INFO) << "Data size: " << std::dec << _data_half_0.size();

    _half_id   = 0x00;
    std::vector<HGCROC_CHN_Data> _data_half_1;
    _data_half_1.reserve(38);

    for (auto _line_index=5; _line_index<10; _line_index++){
        int _start_index = _line_index * 40;
        if (_chip_id != _raw_data->at(_start_index)){
            LOG(ERROR) << "Chip ID is not consistent!";
            return;
        }
        if (_line_index==5) {
            _half_id = _raw_data->at(_start_index+1);
        }
        else {
            if (_half_id != _raw_data->at(_start_index+1)){
                LOG(ERROR) << "Half ID is not consistent!";
                return;
            }
        }
        _line_number_vec.push_back(_raw_data->at(_start_index+3));
        _eventid_vec.push_back(
            (_raw_data->at(_start_index+4) << 24) + 
            (_raw_data->at(_start_index+5) << 16) + 
            (_raw_data->at(_start_index+6) << 8) + 
            (_raw_data->at(_start_index+7))
        );
        if (_line_index==5) {
            this->DaqH_1 = (_raw_data->at(_start_index+8) << 24) + 
                         (_raw_data->at(_start_index+9) << 16) + 
                         (_raw_data->at(_start_index+10) << 8) + 
                         (_raw_data->at(_start_index+11));
        } else {
            _data_half_1.push_back(get_chn_data(_raw_data->at(_start_index+8), _raw_data->at(_start_index+9), _raw_data->at(_start_index+10), _raw_data->at(_start_index+11)));
        }
        for (auto _inline_index=3; _inline_index<9; _inline_index++) {
            _data_half_1.push_back(get_chn_data(_raw_data->at(_start_index+(_inline_index*4)), _raw_data->at(_start_index+(_inline_index*4)+1), _raw_data->at(_start_index+(_inline_index*4)+2), _raw_data->at(_start_index+(_inline_index*4)+3)));
        }
        if (_line_index==9){
            CRC_32_1 = (
                (_raw_data->at(_start_index+36) << 24) + 
                (_raw_data->at(_start_index+37) << 16) + 
                (_raw_data->at(_start_index+38) << 8) + 
                (_raw_data->at(_start_index+39))
            );
        } else {
            _data_half_1.push_back(get_chn_data(_raw_data->at(_start_index+36), _raw_data->at(_start_index+37), _raw_data->at(_start_index+38), _raw_data->at(_start_index+39)));
        }
    }

    // LOG(INFO) << "Half ID: " << std::hex << (int)_half_id;
    // LOG(INFO) << "Line number: " << std::hex << (int)_line_number_vec.at(5) << " " << std::hex << (int)_line_number_vec.at(6) << " " << std::hex << (int)_line_number_vec.at(7) << " " << std::hex << (int)_line_number_vec.at(8) << " " << std::hex << (int)_line_number_vec.at(9);
    // LOG(INFO) << "CRC: " << std::hex << CRC_32_1;
    // LOG(INFO) << "Event ID: " << std::dec << _eventid_vec.at(5) << " " << std::dec << _eventid_vec.at(6) << " " << std::dec << _eventid_vec.at(7) << " " << std::dec << _eventid_vec.at(8) << " " << std::dec << _eventid_vec.at(9);
    // LOG(INFO) << "DAQH: " << std::hex << this->DaqH_1;
    // LOG(INFO) << "DaqH Hd: " << std::dec << (int)this->get_DaqH_Hd(1);
    // LOG(INFO) << "DaqH BC: " << std::dec << (int)this->get_DaqH_BC(1);
    // LOG(INFO) << "DaqH EC: " << std::dec << (int)this->get_DaqH_EC(1);
    // LOG(INFO) << "DaqH OB: " << std::dec << (int)this->get_DaqH_OB(1);
    // LOG(INFO) << "DaqH H1: " << std::dec << (int)this->get_DaqH_H1(1);
    // LOG(INFO) << "DaqH H2: " << std::dec << (int)this->get_DaqH_H2(1);
    // LOG(INFO) << "DaqH H3: " << std::dec << (int)this->get_DaqH_H3(1);
    // LOG(INFO) << "DaqH Tr: " << std::dec << (int)this->get_Daq_Tr(1);
    // LOG(INFO) << "Data size: " << std::dec << _data_half_1.size();

    this->_data.insert(this->_data.end(), _data_half_0.begin(), _data_half_0.end());
    this->_data.insert(this->_data.end(), _data_half_1.begin(), _data_half_1.end());

    for (auto _chn_index=0; _chn_index<this->_data.size(); _chn_index++){
        _data.at(_chn_index).chn_id = _chn_index;
        // LOG(INFO) << "Channel ID: " << std::dec << _data.at(_chn_index).chn_id << " Tc: " << this->_data.at(_chn_index).Tc << " Tp: " << this->_data.at(_chn_index).Tp << " Val: " << this->_data.at(_chn_index).val_10b_0 << " " << this->_data.at(_chn_index).val_10b_1 << " " << this->_data.at(_chn_index).val_10b_2;
    }

    this->EventID = _eventid_vec.at(0);
    this->ChipID = _chip_id;
}

HGCROC_Frame::HGCROC_CHN_Data HGCROC_Frame::get_chn_data(Byte_t& _byte_0, Byte_t& _byte_1, Byte_t& _byte_2, Byte_t& _byte_3){
    HGCROC_Frame::HGCROC_CHN_Data _chn_data;
    _chn_data.Tc = (_byte_0 & 0x80) >> 7;
    _chn_data.Tp = (_byte_0 & 0x40) >> 6;
    _chn_data.val_10b_0 = ((_byte_0 & 0x3F) << 4) + ((_byte_1 & 0xF0) >> 4);
    _chn_data.val_10b_1 = ((_byte_1 & 0x0F) << 6) + ((_byte_2 & 0xFC) >> 2);
    _chn_data.val_10b_2 = ((_byte_2 & 0x03) << 8) + _byte_3;
    return _chn_data;
}

uint16_t HGCROC_Frame::recover_tot(uint16_t _original_val){
    // check the bit 10
    bool _is_MSB_cut = (_original_val & 0x0200) >> 9;
    uint16_t _effetive_val = _original_val & 0x01FF;
    if (_is_MSB_cut) {
        return _effetive_val << 2;
    } else {
        return _effetive_val;
    }
}