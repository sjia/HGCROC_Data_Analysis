#pragma once
#include "TObject.h"
#include "TVector.h"
#include "TMath.h"
#include "easylogging++.h"


class HGCROC_Frame: public TObject{
public:
    struct HGCROC_CHN_Data{
        int chn_id = -1;
        bool Tc = false;
        bool Tp = false;
        uint16_t val_10b_0 = 0;
        uint16_t val_10b_1 = 0;
        uint16_t val_10b_2 = 0;
    };

public:
    HGCROC_Frame(std::vector<Byte_t>* _raw_data);
    HGCROC_Frame():TObject(){};
    ~HGCROC_Frame(){};

    HGCROC_CHN_Data get_chn_data(Byte_t& _byte_0, Byte_t& _byte_1, Byte_t& _byte_2, Byte_t& _byte_3);

    inline uint8_t get_DaqH_Hd(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0xF0000000) >> 28;
        } else {
            return (this->DaqH_0 & 0xF0000000) >> 28;
        }
    }
    inline uint16_t get_DaqH_BC(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x0FFF0000) >> 16;
        } else {
            return (this->DaqH_0 & 0x0FFF0000) >> 16;
        }
    }
    inline uint8_t get_DaqH_EC(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x0000FC00) >> 10;
        } else {
            return (this->DaqH_0 & 0x0000FC00) >> 10;
        }
    }
    inline uint8_t get_DaqH_OB(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x00000380) >> 7;
        } else {
            return (this->DaqH_0 & 0x00000380) >> 7;
        }
    }
    inline bool get_DaqH_H1(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x00000040) >> 6;
        } else {
            return (this->DaqH_0 & 0x00000040) >> 6;
        }
    }
    inline bool get_DaqH_H2(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x00000020) >> 5;
        } else {
            return (this->DaqH_0 & 0x00000020) >> 5;
        }
    }
    inline bool get_DaqH_H3(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x00000010) >> 4;
        } else {
            return (this->DaqH_0 & 0x00000010) >> 4;
        }
    }
    inline uint8_t get_Daq_Tr(bool _half_id) {
        if (_half_id) {
            return (this->DaqH_1 & 0x0000000F);
        } else {
            return (this->DaqH_0 & 0x0000000F);
        }
    }

    uint16_t recover_tot(uint16_t _original_val);
public:
    std::vector<HGCROC_CHN_Data> _data;
    uint32_t CRC_32_0, CRC_32_1;
    uint32_t DaqH_0, DaqH_1;
    uint32_t EventID;
    uint8_t ChipID;

ClassDef(HGCROC_Frame, 1);
};
