rule cmake_script:
    conda:
        'general'
    output:
        'build/{script_exec}'
    input:
        'scripts/SJH_{script_exec}.cxx'
    shell:
        'cmake --build build --target {wildcards.script_exec} -- -j 4'