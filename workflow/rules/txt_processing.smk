import os

# get all data files in data/
samples = [f.replace('TestLog_', '').replace('.txt', '') for f in os.listdir('data') if f.endswith('.txt')]

rule rootifier:
    conda:
        'general'
    log:
        'logs/rootifier/{sample}.log'
    output:
        'dump/rootifier/data_{sample}.root'
    input:
        executable='build/rootifier',
        data='data/TestLog_{sample}.txt'
    shell:
        '''
        {input.executable} -i {input.data} -o {output} > {log}
        '''

rule rootifier_in_folder:
    conda:
        'general'
    log:
        'logs/rootifier/{folder}/{sample}.log'
    output:
        'dump/rootifier/{folder}/data_{sample}.root'
    input:
        executable='build/rootifier',
        data='data/{folder}/{sample}.txt'
    shell:
        '''
        {input.executable} -i {input.data} -o {output} > {log}
        '''

rule rootify_all:
    conda:
        'general'
    input:
        expand('dump/rootifier/data_{sample}.root', sample=samples)
    output:
        'dump/rootifier/all.done'
    shell:
        '''
        touch {output}
        '''

rule rootify_scan:
    input:
        executable='build/rootifier',
        samples=lambda wildcards: [f.replace("data/", "dump/rootifier/".format(wildcards.scan_folder)).replace(".txt", ".root").replace("Scanning", "data_Scanning") for f in glob(f"data/{wildcards.scan_folder}/*.txt")]
    output:
        'dump/rootifier/{scan_folder}/all.done'
    conda:
        'general'
    log:
        'logs/rootifier/{scan_folder}/all.log'
    params:
        scan_folder=lambda wildcards: wildcards.scan_folder
    shell:
        """
        touch {output}
        """