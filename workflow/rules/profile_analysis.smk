import os
from glob import glob

# get all data files in data/
samples = [f.replace('TestLog_', '').replace('.txt', '') for f in os.listdir('data') if f.endswith('.txt')]

rule raw_analysis:
    conda:
        'general'
    log:
        'logs/raw_analysis/analysis_{sample}.log'
    input:
        executable='build/rawanalysis',
        data='dump/rootifier/data_{sample}.root'
    output:
        'dump/raw_analysis/analysis_{sample}.root'
    shell:
        '{input.executable} -i {input.data} -o {output} > {log}'

rule raw_analysis_in_folder:
    conda:
        'general'
    log:
        'logs/raw_analysis/{folder}/analysis_{sample}.log'
    input:
        executable='build/rawanalysis',
        data='dump/rootifier/{folder}/data_{sample}.root'
    output:
        'dump/raw_analysis/{folder}/analysis_{sample}.root'
    shell:
        '{input.executable} -i {input.data} -o {output} > {log}'

rule raw_analysis_all:
    input:
        expand('dump/raw_analysis/analysis_{sample}.root', sample=samples)
    output:
        'dump/raw_analysis/all.done'
    shell:
        'touch {output}'

rule raw_analysis_scan_all:
    input:
        executable='build/rawanalysis',
        samples=lambda wildcards: [f.replace("data/", "dump/raw_analysis/".format(wildcards.scan_folder)).replace(".txt", ".root").replace("Scanning", "analysis_Scanning") for f in glob(f"data/{wildcards.scan_folder}/*.txt")]
    output:
        'dump/raw_analysis/{scan_folder}/all.done'
    conda:
        'general'
    log:
        'logs/raw_analysis/{scan_folder}/all.log'
    shell:
        """
        touch {output}
        """

rule delay_scan:
    conda:
        'general'
    log:
        'logs/scan_analysis/{scan_folder}/delay_scan.log'
    input:
        executable='build/scananalysis',
        done_flag='dump/rootifier/{scan_folder}/all.done'
    output:
        'dump/scan_analysis/{scan_folder}.root'
    shell:
        """
        {input.executable} -i dump/rootifier/{wildcards.scan_folder} -o {output}
        """