cmake_minimum_required(VERSION 3.12)
set(CMAKE_CXX_STANDARD 17)
project(hgcroc_analysis)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DELPP_NO_DEFAULT_LOG_FILE")

find_package(ROOT REQUIRED COMPONENTS 
    Core 
    Tree
    Hist
    Gpad
    Graf3d
    Thread
    RIO
    RooFitCore
    RooFit
    Graf
    Matrix
)

# Add ROOT include directories
include_directories(${ROOT_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_library(generallib SHARED 
    src/easylogging++.cc
    include/csv.h
    include/SJH_frame.h
    src/SJH_frame.cxx
)

target_link_libraries(generallib PUBLIC 
    ROOT::Core
    ROOT::Tree
    ROOT::Hist
    ROOT::Gpad
    ROOT::Graf3d
    ROOT::RooFitCore
    ROOT::RooFit
    ROOT::RIO
    ROOT::Thread
    ROOT::Graf
    ROOT::Matrix
)

set_target_properties(generallib PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib
)

add_executable(rootifier scripts/SJH_rootifier.cxx)
add_executable(rawanalysis scripts/SJH_rawanalysis.cxx)
add_executable(scananalysis scripts/SJH_scananalysis.cxx)
add_executable(multiscan scripts/SJH_multiscan.cxx)

target_link_libraries(rootifier generallib)
target_link_libraries(rawanalysis generallib)
target_link_libraries(scananalysis generallib)
target_link_libraries(multiscan generallib)

# Dictionary Generation
ROOT_GENERATE_DICTIONARY(G__dict ../include/SJH_frame.h LINKDEF include/SJH_LinkDef.h)

# Add dictionary to our library
add_library(Dict SHARED G__dict.cxx)
target_link_libraries(Dict PUBLIC ROOT::Core)

# Link the dictionary to executables
target_link_libraries(rootifier Dict)
target_link_libraries(rawanalysis Dict)
target_link_libraries(scananalysis Dict)
target_link_libraries(multiscan Dict)
