# HGCROC Data Analysis

[![Snakemake](https://img.shields.io/badge/snakemake-7.32-brightgreen.svg)](https://snakemake.readthedocs.io) [![CERN ROOT](https://img.shields.io/badge/CERN-ROOT-blue.svg)](https://root.cern/) [![CMake](https://img.shields.io/badge/CMake-3.22-green.svg)](https://cmake.org/)


## Requirements

**This project is developed and tested on Ubuntu 22.04**

- [CERN ROOT](https://root.cern.ch/downloading-root) (6.28/06)
- [CMake](https://cmake.org/download/) (3.22.1)
- [GCC](https://gcc.gnu.org/install/) (11.4.0 x86_64-linux-gnu)
- [Snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) (7.32.4)

## Thrid-party libraries

You don't need to install these libraries. They are included in the project as header-only libraries.

- [fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser)
- [easyloggingpp](https://github.com/abumq/easyloggingpp)

## Building

This project uses CMake to build the executables. To build the project, run the following commands:

First create a build directory:

```bash
mkdir build
```

Configure the project:

```bash
cmake -S . -B build
```

Build the project:

```bash
cmake --build build --target all
```

## Running

!!! attention
    You must have data files in the `data` directory to run the analysis.

A typical project directory structure is as follows:

```bash
.
├── build
│   ├── CMakeFiles
│   └── lib
├── data
|   └── TestLog_xxxxxxxx_xxxxxx_xxx.txt
├── dump
├── include
├── logs
├── scripts
├── src
└── workflow
    ├── configs
    └── rules
```

I use `snakemake` to run the analysis. To run the analysis, run the following command:

- rootify all data files in `data/`
  ```bash
  snakemake --cores <number of cores> dump/rootifier/all.done
  ```
- get the basic information of the data files
  ```bash
  snakemake --cores <number of cores> dump/raw_analysis/all.done
  ```
- plot the delay scan results for all channels
  ```bash
  snakemake --cores <number of cores> dump/scan_analysis/<scan_folder>.root
  ```


